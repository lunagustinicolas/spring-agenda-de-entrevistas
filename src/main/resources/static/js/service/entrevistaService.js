var entrevistaService = (function () {
    
    const eliminar = (id) => {
        return $.ajax({
            url: `http://localhost:8081/api/entrevistas/${id}`,
            type: 'DELETE'
        });
    };

    return {
        eliminar: eliminar
    };
})();
 