package com.somospnt.agendadeentrevistas.repository;

import com.somospnt.agendadeentrevistas.domain.Entrevista;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistaRepository extends JpaRepository<Entrevista, Long> {

    List<Entrevista> findByUsuarioUsername(String username);

    void deleteByIdAndUsuarioUsername(Long id, String username);
}
