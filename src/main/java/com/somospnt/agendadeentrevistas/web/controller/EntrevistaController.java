package com.somospnt.agendadeentrevistas.web.controller;

import com.somospnt.agendadeentrevistas.business.service.EntrevistaService;
import com.somospnt.agendadeentrevistas.domain.Entrevista;
import java.time.ZonedDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequiredArgsConstructor
@Controller
public class EntrevistaController {

    private final EntrevistaService entrevistaService;

    @RequestMapping("/entrevistas")
    public String verEntrevistas(Model model) {
        List<Entrevista> entrevistas = entrevistaService.buscarPorUsuarioLogueado();
        ZonedDateTime hoy = ZonedDateTime.now();
        model.addAttribute("entrevistas", entrevistas);
        model.addAttribute("hoy", hoy);
        return "entrevistas";
    }

    @RequestMapping("/crearEntrevista")
    public String crearEntrevista(@RequestParam String nombre, @RequestParam String fecha, @RequestParam String hora, Model model) {

        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado(nombre);
        
        if (!hora.isEmpty() && !fecha.isEmpty()) {
            ZonedDateTime fechaPactada = ZonedDateTime.parse(fecha + "T" + hora + ":00-03:00");
            entrevista.setFechaPactada(fechaPactada);
        }

        entrevistaService.crear(entrevista);
        return "redirect:/entrevistas";
    }

}
