package com.somospnt.agendadeentrevistas.web.controller.rest;

import com.somospnt.agendadeentrevistas.business.service.EntrevistaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class EntrevistaRestController {

    @Autowired
    private EntrevistaService entrevistaService;

    @DeleteMapping("/entrevistas/{id}")
    public void eliminarEntrevista(@PathVariable Long id) {
        entrevistaService.eliminarPorId(id);
    }

}
