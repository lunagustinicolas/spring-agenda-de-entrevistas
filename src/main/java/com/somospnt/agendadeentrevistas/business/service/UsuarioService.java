package com.somospnt.agendadeentrevistas.business.service;

import com.somospnt.agendadeentrevistas.domain.Usuario;
import com.somospnt.agendadeentrevistas.repository.UsuarioRepository;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Transactional
public class UsuarioService {
    
    private final UsuarioRepository usuarioRepository;

    public Usuario buscarPorUsername(String username) {
        return usuarioRepository.findByUsername(username);
    }
    
}
