package com.somospnt.agendadeentrevistas.business.service;

import com.somospnt.agendadeentrevistas.domain.Entrevista;
import com.somospnt.agendadeentrevistas.domain.Usuario;
import com.somospnt.agendadeentrevistas.repository.EntrevistaRepository;
import java.util.List;
import javax.transaction.Transactional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@RequiredArgsConstructor
@Service
@Transactional
@Validated
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EntrevistaService {

    private final EntrevistaRepository entrevistaRepository;
    private final UsuarioService usuarioService;
    
    @PreAuthorize("isAuthenticated()")
    public void crear(@Valid Entrevista entrevista) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Usuario usuario = usuarioService.buscarPorUsername(username);
        entrevista.setUsuario(usuario);
        entrevistaRepository.save(entrevista);
    }
    
    @PreAuthorize("isAuthenticated()")
    public void eliminarPorId(Long id) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Entrevista entrevistaABorrar = entrevistaRepository.findById(id).orElse(null);
        if (entrevistaABorrar.getUsuario().getUsername().equals(username)) {
            entrevistaRepository.deleteById(id);
        }else{
            throw new SecurityException("No se puede eliminar");
        }
    }
    
    @PreAuthorize("isAuthenticated()")
    public List<Entrevista> buscarPorUsuarioLogueado() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return entrevistaRepository.findByUsuarioUsername(username);
    }

}
