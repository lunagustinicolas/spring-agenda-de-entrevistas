package com.somospnt.agendadeentrevistas.domain;

import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Entity
public class Entrevista {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Este campo no puede estar vacío")
    private String nombreEntrevistado;
    @Future
    private ZonedDateTime fechaPactada;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
}
