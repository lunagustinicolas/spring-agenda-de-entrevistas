package com.somospnt.agendadeentrevistas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendaDeEntrevistasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendaDeEntrevistasApplication.class, args);
	}
}
