package com.somospnt.agendadeentrevistas.business.service;

import com.somospnt.agendadeentrevistas.AgendaDeEntrevistasApplication;
import com.somospnt.agendadeentrevistas.domain.Usuario;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AgendaDeEntrevistasApplication.class)
public class UsuarioServiceTest {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Test
    public void buscarPorUsername_conUsernameValido_retornaUsuarioDeseado() {
        Usuario usuario = usuarioService.buscarPorUsername("santi");

        assertNotNull(usuario);
    }
}
