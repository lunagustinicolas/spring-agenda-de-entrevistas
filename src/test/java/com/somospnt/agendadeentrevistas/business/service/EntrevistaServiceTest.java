package com.somospnt.agendadeentrevistas.business.service;

import com.somospnt.agendadeentrevistas.AgendaDeEntrevistasApplication;
import com.somospnt.agendadeentrevistas.domain.Entrevista;
import com.somospnt.agendadeentrevistas.repository.EntrevistaRepository;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AgendaDeEntrevistasApplication.class)
@Transactional
public class EntrevistaServiceTest {

    @Autowired
    private EntrevistaService entrevistaService;
    @Autowired
    private EntrevistaRepository entrevistaRepository;

    @Test
    @WithMockUser(username = "agus")
    public void crear_conUsuarioLogueadoYEntrevistaValida_seActualizaId() {
        ZoneId zoneId = ZoneId.of("UTC-3");
        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado("Geronimo");
        entrevista.setFechaPactada(ZonedDateTime.of(2025, 2, 10, 11, 45, 59, 1234, zoneId));
        entrevistaService.crear(entrevista);

        assertNotNull(entrevista.getId());
    }

    @Test
    @WithMockUser(username = "agus")
    public void crear_conUsuarioLogueadoYNombreEntrevistadoInvalido_lanzaExcepcion() {
        ZoneId zoneId = ZoneId.of("UTC-3");
        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado(" ");
        entrevista.setFechaPactada(ZonedDateTime.of(3000, 2, 10, 11, 45, 59, 1234, zoneId));

        Exception exception = assertThrows(ConstraintViolationException.class, () -> {
            entrevistaService.crear(entrevista);
        });

        assertTrue(exception.getMessage().contains("Este campo no puede estar vacío"));
    }

    @Test
    @WithMockUser(username = "agus")
    public void crear_conUsuarioLogueadoYFechaPactadaInvalida_lanzaExcepcion() {
        ZoneId zoneId = ZoneId.of("UTC-3");
        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado("Juan");
        entrevista.setFechaPactada(ZonedDateTime.of(2020, 2, 3, 11, 45, 59, 1234, zoneId));

        Exception exception = assertThrows(ConstraintViolationException.class, () -> {
            entrevistaService.crear(entrevista);
        });

        assertTrue(exception.getMessage().contains("debe ser una fecha futura"));
    }

    @Test
    @WithMockUser(username = "agus")
    public void eliminarPorId_conUsuarioLogueadoEIdValido_seEliminaEntrevista() {
        entrevistaService.eliminarPorId(1L);

        Entrevista entrevistaBorrada = entrevistaRepository.findById(1L).orElse(null);
        assertNull(entrevistaBorrada);
    }

    @Test
    @WithMockUser(username = "agus")
    public void eliminarPorId_conUsuarioLogueadoEIdInvalido_lanzaExcepcion() {
        assertThrows(NullPointerException.class, () -> entrevistaService.eliminarPorId(7L));
    }

    @Test
    @WithMockUser(username = "santi")
    public void eliminarPorId_conUsuarioLogueadoYEntrevistaNoPropia_lanzaSecurityException() {
        assertThrows(SecurityException.class, () -> entrevistaService.eliminarPorId(1L));
    }

    @Test
    @WithMockUser(username = "agus")
    public void buscarPorUsuarioLogueado_conUsuarioLogueado_retornaEntrevistas() {
        List<Entrevista> entrevistas = entrevistaService.buscarPorUsuarioLogueado();

        assertFalse(entrevistas.isEmpty());
    }

    /* ------------------------------ Usuario no logueado --------------------------------- */
    @Test
    public void buscarPorUsuarioLogueado_conUsuarioNoLogueado_lanzaExcepcion() {
        assertThrows(AuthenticationCredentialsNotFoundException.class, () -> entrevistaService.buscarPorUsuarioLogueado());
    }

    @Test
    public void eliminarPorId_conUsuarioNoLogueado_lanzaExcepcion() {
        assertThrows(AuthenticationCredentialsNotFoundException.class, () -> entrevistaService.eliminarPorId(5L));
    }

    @Test
    public void crear_conUsuarioNoLogueado_lanzaExcepcion() {
        ZoneId zoneId = ZoneId.of("UTC-3");
        Entrevista entrevista = new Entrevista();
        entrevista.setNombreEntrevistado("Geronimo");
        entrevista.setFechaPactada(ZonedDateTime.of(2025, 2, 10, 11, 45, 59, 1234, zoneId));

        assertThrows(AuthenticationCredentialsNotFoundException.class, () -> entrevistaService.crear(entrevista));
    }
}
